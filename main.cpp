#include <iostream>
#include <fstream>
#include <string>

#include "nat.h"
#include "types.h"

Entry parse_entry(const std::string& input) {
    Entry ret;
    size_t position;

    position = input.find(','); // find comma
    std::string src = input.substr(0, position);
    std::string dst = input.substr(position + 1, input.size() - position - 1);

    position = dst.find(':');
    ret.dst_addr.ip = dst.substr(0, position);
    ret.dst_addr.port = uint16_t(std::stoi(dst.substr(position + 1, dst.length() - position - 1)));

    position = src.find(':');
    std::string src_ip = src.substr(0, position);
    std::string src_port = src.substr(position + 1, src.length() - position - 1);
    if (src_ip == "*") {
        ret.any_src_ip = true;
        ret.src_addr.port = uint16_t(std::stoi(src_port));
    } else if (src_port == "*") {
        ret.any_src_port = true;
        ret.src_addr.ip = src_ip;
    } else {
        ret.src_addr.ip = src_ip;
        ret.src_addr.port = uint16_t(std::stoi(src_port));
    }

    return ret;
}

int main() {
    NAT translator; // translator class
    std::string line;

    // Load from file NAT for rules
    std::ifstream nat_file("NAT");
    if (nat_file.is_open()) {
        while (std::getline(nat_file, line)) {
            translator.add(parse_entry(line));
        }
        nat_file.close();
    } else {
        std::cout<<"Cannot find file NAT"<<std::endl;
    }

    // Load flow
    std::ifstream flow_file("FLOW");
    std::ofstream output("OUTPUT");
    if (flow_file.is_open() && output.is_open()) {
        while (std::getline(flow_file, line)) {
            Address input;
            size_t position = line.find(':');
            input.ip = line.substr(0, position);
            input.port = uint16_t(std::stoi(line.substr(position + 1, line.size() - position - 1)));

            Address result;
            if (translator.translate(result, input)) {
                // find match
                output<<input.ip<<":"<<int(input.port)<<" -> "<<result.ip<<":"<<int(result.port)<<std::endl;
            } else {
                output<<"No nat match for "<<input.ip<<":"<<int(input.port)<<std::endl;
            }
        }
        flow_file.close();
        output.close();
    } else {
        std::cout<<"Cannot find files"<<std::endl;
    }


    return 0;
}
