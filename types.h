/* Types */

#ifndef TYPES_H
#define TYPES_H

#include <string>
#include <stdint.h>
#include <unordered_map>

typedef std::string IP;
typedef uint16_t Port;

// Represent a ip:port pair
struct Address {
    IP ip;
    Port port;

    // Override == for hashmap
    bool operator== (const Address& arg) const {
        return (ip == arg.ip) && (port == arg.port);
    }
};

// A hash for address struct
struct AddressHash {
    std::hash<std::string> string_hash;
    std::hash<uint16_t> u16_hash;

    std::size_t operator() (const Address& key) const {
        return u16_hash(key.port) + string_hash(key.ip);
    }
};

struct Entry {
    Address src_addr;
    Address dst_addr;

    bool any_src_ip;
    bool any_src_port;
};

#endif
