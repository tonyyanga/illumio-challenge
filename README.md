NAT coding challenge
=======================

### How I tested my solution?

1. `g++ -Wall --std=c++11 *.cpp` returned no warning

2. I generated data with test.py, and received satisfying output

3. I also run the sample test cases provided in the email and validated that everything works (with a wrong case in sample).

### Interesting design?

A design choice is whether to use a trie or a hashmap for query. A trie may save memory compared to a hashmap, but I used a hashmap because of simplicity. Also, as a trie requires more steps of lookup, it has even less locality than a hashmap.

Given that I used a hashmap, the hashing function for a general NAT entry will be interesting since it involves an IP address and a port. My current implementation is naive due to time limit, but I think the best approach will be to threat IP and port as a (32+16) bit unsigned integer.

Also, in real life, NAT entry can be given with a port range or IP range (e.g. with subnet mask). In that case a simple hashmap approach will not work. In that case a customized trie can an much better option for IP addresses.

### Refinement / optimizations?

I was in a hurry with the IO part, i.e. main.cpp, so I left redundant code, particularly with parsing <ip>:<port> string. If I had more time, I would put parsing string input ina function.

Since we are dealing with IP address, I prefer to use `uint32_t` than `std::string` to represent it. However, I was uncertain if I can use `inet_aton`, `inet_ntoa` and I didn't have time to write my own conversions. I used `std::string` instead.

I hope I could better document the IO part of the code, i.e. main.cpp.

I also hope I had time to write a Makefile, and actually use `diff` to run the test script.

### Assumed about challenge?

I assumed an priority order for lookup in NAT rules:

1. Rules matching any port > rules matching any IP > other rules.

2. If there are conflicting rules in the same category, the latter it shows up in NAT file, the higher priority it gets.

These are arbitary decisions, but since it is not specified in the question, I chose to make it this way.
