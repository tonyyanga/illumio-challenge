import random
import socket
import struct

addr_list = []

f = open('FLOW', 'w')
for p in range(100):
    ip = socket.inet_ntoa(struct.pack('>I', random.randint(1, 0xffffffff)))
    port = p + 10000
    addr_list.append(ip + ":" + str(port))
    f.write(ip + ":" + str(port) + '\n')
f.close()

f = open('NAT', 'w')
for p in range(10010, 10020):
    f.write("*:" + str(p) + ",107.51.11.25:787\n")
for addr in addr_list[70:]:
    f.write(addr + ',88.45.15.65:887\n')
f.close()

