/* NAT translation class */

#ifndef NAT_H
#define NAT_H

#include <unordered_map>

#include "types.h"

/* NAT Translator */
class NAT {
private: // Data members
    // Maps from source address to destination address
    // NAT entries allowing arbitary ports
    std::unordered_map<IP, Address> ip_entries;
    // NAT entries allowing arbitary IPs
    std::unordered_map<Port, Address> port_entries;
    // All other NAT entries
    std::unordered_map<Address, Address, AddressHash> regular_entries;

public: // Translation functions
    // Add entry to NAT translator map
    void add(const Entry& entry);

    /* Translate from src and write result to dst, and return true
     * if no translation available, return false
     * Look up in following order:
     * IP-only, Port-only, regular */
    bool translate(Address& dst, const Address& src);
};

#endif
