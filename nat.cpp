#include "nat.h"

/* Implementations for NAT class */

void NAT::add(const Entry& entry) {
    if (entry.any_src_port) {
        // Add to IP-only map
        ip_entries[entry.src_addr.ip] = entry.dst_addr;
    } else if (entry.any_src_ip) {
        // Add to port-only map
        port_entries[entry.src_addr.port] = entry.dst_addr;
    } else {
        // Regular fully defined entry
        regular_entries[entry.src_addr] = entry.dst_addr;
    }
}

bool NAT::translate(Address& dst, const Address& src) {
    /* Look up in following order:
     * IP-only, Port-only, regular */
    if (ip_entries.count(src.ip)) {
        dst = ip_entries[src.ip];
        return true;
    } else if (port_entries.count(src.port)) {
        dst = port_entries[src.port];
        return true;
    } else if (regular_entries.count(src)) {
        dst = regular_entries[src];
        return true;
    } else {
        return false;
    }
}
